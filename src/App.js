import logo from "./logo.svg";
import "./App.css";
import Ex_Shoe_Redux from "./ex_shoe_redux/Ex_Shoe_Redux";

function App() {
  return (
    <div className="App">
      <Ex_Shoe_Redux />
    </div>
  );
}

export default App;
