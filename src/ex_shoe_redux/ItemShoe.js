import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_SHOE, GET_DETAIL } from "./redux/constant/constant";

class ItemShoe extends Component {
  render() {
    return (
      <div className="col-6">
        <div className="card text-left">
          <img
            style={{ width: "10vw" }}
            className="card-img-top"
            src={this.props.dataShoe.image}
          />
          <div className="card-body text-center">
            <h4 className="card-title">{this.props.dataShoe.price}</h4>
            <button
              onClick={() => {
                this.props.handleViewDetail(this.props.dataShoe);
              }}
              className="btn btn-success"
            >
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.handleAddProduct(this.props.dataShoe);
              }}
              className="btn btn-warning"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleAddProduct: (itemShoe) => {
      let action = {
        type: ADD_SHOE,
        payload: itemShoe,
      };
      dispatch(action);
    },
    handleViewDetail: (itemShoe) => {
      let action = {
        type: GET_DETAIL,
        payload: itemShoe,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
