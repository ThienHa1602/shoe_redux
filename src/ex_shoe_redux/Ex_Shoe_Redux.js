import React, { Component } from "react";
import CartShoe from "./CartShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_Shoe_Redux extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-8">
            <CartShoe />
          </div>
          <div className="col-4">
            <ListShoe />
          </div>
        </div>
        <DetailShoe />
      </div>
    );
  }
}
