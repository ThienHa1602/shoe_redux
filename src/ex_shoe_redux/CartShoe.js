import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY, DELETE_SHOE } from "./redux/constant/constant";

class CartShoe extends Component {
  renderContentCart = () => {
    return this.props.cartShoe.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img style={{ width: 80 }} src={item.image} />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-primary"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemoveShoe(item.id);
              }}
              className="btn  border-danger text-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Image</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderContentCart()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cartShoe: state.shoeReducer.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleRemoveShoe: (id) => {
      let action = {
        type: DELETE_SHOE,
        payload: id,
      };
      dispatch(action);
    },
    handleChangeQuantity: (id, luaChon) => {
      let action = {
        type: CHANGE_QUANTITY,
        payload: id,
        luaChon,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);
