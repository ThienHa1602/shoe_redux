import { shoeArr } from "../../dataShoe";
import {
  ADD_SHOE,
  CHANGE_QUANTITY,
  DELETE_SHOE,
  GET_DETAIL,
} from "../constant/constant";

let initialState = {
  shoeArr: shoeArr,
  shoeDetail: shoeArr[3],
  cart: [],
};
export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DETAIL: {
      return { ...state, shoeDetail: action.payload };
    }
    case ADD_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }
    case DELETE_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((shoe) => {
        return shoe.id == action.payload;
      });
      cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((shoe) => {
        return shoe.id == action.payload;
      });
      cloneCart[index].soLuong = cloneCart[index].soLuong + action.luaChon;
      cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    default: {
      return state;
    }
  }
};
